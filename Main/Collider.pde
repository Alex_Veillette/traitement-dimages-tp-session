class Collider
{ 
  boolean circleToCirle(PVector locationA, PVector locationB, Enemy enemy, float rA, float rB)
  {
    if (PVector.dist (locationA, locationB) < (rA + rB))
    {
      enemy.setColor(color(20, 20, 20));
      return true;
    }
    else
    {
      enemy.setColor(enemy.baseFillColor);
      return false;
    }
  }
  
  PVector checkEdges(PVector location, float size)
  {
    if (location.x < -200) 
    {
      location.x = width - (size);
    } 
    else if (location.x + (size) > width + 200) 
    {
      location.x = -200;
    }
    else if (location.y < -200) 
    {
      location.y = height - (size);
    } 
    else if (location.y + (size) > height + 200) 
    {
      location.y = -200;
    }
    
    return location;
  }
  
  boolean circleToSquare()
  {
    return false;
  }
  
  boolean squareToSquare()
  {
    return false;
  }
}
