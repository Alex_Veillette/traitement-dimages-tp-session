public class EnemyFactory extends Factory {
  
  public Enemy create(String name, PVector location) {
    Enemy enemy = null;
    switch (name) {
      case "wizard":
        enemy = new Wizard(location, new PVector(random(-5, 5), random(-5, 5)), color(20, 20, 250));
        break;
        
      case "slime":
        enemy = new Slime(location, new PVector(random(-5, 5), random(-5, 5)), color(20, 250, 20));
        break;
    }
    return enemy;
  }
}
