public static class Camera
{
  static PVector location = new PVector();
  
  public static PVector getLocation()
  {
    return location;
  }
  
  public static void setLocation(PVector _location)
  {
    location = _location;
  }
}
