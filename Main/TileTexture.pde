public class TileTexture
{
  String tileSetPath = "tileset.png";
  PImage tileSet = loadImage(tileSetPath);
  int textureWidth, textureHeight;
  
  public TileTexture()
  {
    tileSet.resize(224, 1472);
    
    textureWidth = 32;
    textureHeight = 32;
  }
  
  public void setTileSet(PImage tileSet)
  {
    this.tileSet = tileSet;
  }
}
