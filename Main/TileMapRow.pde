public class TileMapRow
{
  private ArrayList<TileMapCell> columns;
  
  public TileMapRow()
  {
    columns = new ArrayList<TileMapCell>();
  }
  
  public TileMapCell getMapCell(int index)
  {
    return columns.get(index);
  }
}
