abstract class Enemy extends GraphicObject
{
  Player player;
  RandomGenerator randomGenerator;
  Collider collider;
  ArrayList<Projectile> projectileList;
  
  Enemy()
  {
    this.location = new PVector(0, 0);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    
    this.mass = 64;
    this.size = mass * 0.5;
    this.G = 1;
    
    randomGenerator = new RandomGenerator();
    collider = new Collider();
    projectileList = new ArrayList<Projectile>();
  }
  
  abstract void createProjectiles(int quantity);
  
  void update(float delta)
  {
    location = collider.checkEdges(location, size);
    
    velocity.add (acceleration);
    velocity.limit(topSpeed);
    location.add (velocity);

    acceleration.mult (0); 
  }
  
  void display()
  {
    
  }
  
  void projectileUpdate(float delta)
  {
    for (int i = 0; i < projectileList.size(); i++) 
    {
      for (int j = 0; j < projectileList.size(); j++) 
      {
        if (i != j) 
        {
          PVector repelForce = projectileList.get(j).repel(projectileList.get(i));
          projectileList.get(i).location = collider.checkEdges(projectileList.get(i).location, projectileList.get(i).size);
          projectileList.get(i).flock(projectileList);
          projectileList.get(i).applyForce(repelForce);
        }
      }
      PVector attractForce = this.attract(projectileList.get(i));
      projectileList.get(i).applyForce(attractForce);
      projectileList.get(i).update(delta);
    }
  }
  
  void projectileDisplay()
  {
    for (int i = 0; i < projectileList.size(); i++)
    {
      projectileList.get(i).display();
    }
  }
  
  void setColor(color fillColor) {
    
    this.fillColor = fillColor;
  }
  
  PVector attract(Projectile projectile) {
   
    PVector force = PVector.sub(location, projectile.location);
    float distance = force.mag();
    distance = constrain(distance, 50.0, 200.0);
    force.normalize();
    
    float strength = (G * mass * projectile.mass) / (distance * distance);
    force.mult(strength);
    
    return force;
  }
}
