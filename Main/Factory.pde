public abstract class Factory {
 
  public abstract GraphicObject create(String name, PVector location);
}
