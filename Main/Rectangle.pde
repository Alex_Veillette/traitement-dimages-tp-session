class Rectangle extends GraphicObject
{
  private float shapeWidth, shapeHeight;
  float left, right, top, down;
  
  Rectangle()
  {
    location = new PVector();
    setShapeWH(100, 100);
    fillColor = 200;
  }
  
  Rectangle(float shapeWidth, float shapeHeight)
  {
    location = new PVector();
    setShapeWH(shapeWidth, shapeHeight);
  }
  
  Rectangle(float shapeX, float shapeY, float shapeWidth, float shapeHeight)
  {
    location = new PVector(shapeX, shapeY);
    setShapeWH(shapeWidth, shapeHeight);
  }
  
  float getShapeWidth()
  {
    return shapeWidth;
  }
  
  float getShapeHeight()
  {
    return shapeHeight;
  }
  
  void setShapeXY(float shapeX, float shapeY)
  {
    location.x = shapeX;
    location.y = shapeY;
    updateSideLocation();
  }
  
  void setShapeWH(float shapeWidth, float shapeHeight)
  {
    this.shapeWidth = shapeWidth;
    this.shapeHeight = shapeHeight;
    updateSideLocation();
  }
  
  private void updateSideLocation()
  {
    left = location.x;
    right = left + shapeWidth;
    top = location.y;
    down = top + shapeHeight;
  }
  
  void update(float delta)
  {
    updateSideLocation();
  }
  
  void display()
  {
    
  }
}
