public class TileMapCell
{
  private int id;
  
  public int getTileID()
  {
    return id;
  }
  
  public void setTileID(int id)
  {
    this.id = id;
  }
  
  public TileMapCell(int id)
  {
    this.id = id;
  }
}
