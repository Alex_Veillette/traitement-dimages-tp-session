class Altar extends Trap
{
  Altar(PVector location, color fillColor)
  {
    this.location = location;
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    
    this.mass = 32;
    this.size = mass * 0.5;
    
    this.baseFillColor = fillColor;
    this.fillColor = fillColor;
    
    println("altar created");
  }
  
  void update(float delta)
  {
    
  }
  
  void display()
  {
    trapModel();
  }
  
  void createProjectiles(int quantity)
  {
    
  }
  
  void trapModel()
  {
    pushMatrix();
    translate(location.x  - Camera.location.x, location.y  - Camera.location.y);
    fill(fillColor);
    ellipse (0, 0, size, size);
    popMatrix();
  }
}
