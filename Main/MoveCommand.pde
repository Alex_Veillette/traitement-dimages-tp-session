public class MoveCommand{}

public class MoveUp implements ICommand
{
  private Player player;
  
  public MoveUp(Player player)
  {
    this.player = player;
  }
  
  public void execute()
  {
    player.moveUp();
  }
  
  public void unExecute()
  {
    player.stay();
  }
}

public class MoveDown implements ICommand
{
  private Player player;
  
  public MoveDown(Player player)
  {
    this.player = player;
  }
  
  public void execute()
  {
    player.moveDown();
  }
  
  public void unExecute()
  {
    player.stay();
  }
}

public class MoveLeft implements ICommand
{
  private Player player;
  
  public MoveLeft(Player player)
  {
    this.player = player;
  }
  
  public void execute()
  {
    player.moveLeft();
  }
  
  public void unExecute()
  {
    player.stay();
  }
}

public class MoveRight implements ICommand
{
  private Player player;
  
  public MoveRight(Player player)
  {
    this.player = player;
  }
  
  public void execute()
  {
    player.moveRight();
  }
  
  public void unExecute()
  {
    player.stay();
  }
}
