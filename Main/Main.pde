int currentTime;
int previousTime;
int deltaTime;

Game game;

void setup()
{
  size(768, 576, P2D);
  currentTime = millis();
  deltaTime = millis();
  
  game = new Game();
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  background(50);
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int delta)
{
  game.update(delta);
}

void display()
{
  game.display();
}

void keyPressed()
{
  game.setKeyPressed(key);
}

void keyReleased()
{
  game.setKeyReleased(key);
  
  if (key == 'r')
  {
    setup();
  }
}
