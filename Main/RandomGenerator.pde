class RandomGenerator
{
  float randF, randX, randY;
  int randI;
  PVector locationA, locationB;
  
  RandomGenerator()
  {
    locationB = new PVector(0, 0);
  }
  
  void generate(PVector locationA)
  {
    this.locationA = locationA;
    randF = random(1,5);
    randI = int(randF);
    
    switch (randI)
    {
      case 1:
      //Left UP
      locationB.x = random(0, ((locationA.x) - (locationA.x / 3)));
      locationB.y = random(0, ((locationA.y) - (locationA.y / 3)));
      break;
      
      case 2:
      //Left DOWN
      locationB.x = random(0 , ((locationA.x) - (locationA.x / 3)));
      locationB.y = random((locationA.y) + (locationA.y / 3) , height);
      break;
      
      case 3:
      //Right UP
      locationB.x = random((locationA.x) + (locationA.x / 3) , width);
      locationB.y = random(0, ((locationA.y) - (locationA.y / 3)));
      break;
      
      case 4:
      //Right DOWN
      locationB.x = random((locationA.x) + (locationA.x / 3) , width);
      locationB.y = random((locationA.y) + (locationA.y / 3) , height);
      break;
    }
  }
  
  PVector getRandomVector()
  {
    return locationB.get();
  }
}
