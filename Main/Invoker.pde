public class Invoker
{ 
  private ICommand command;
  
  public void setCommand(ICommand command)
  {
    this.command = command;
  }
  
  public void keyPressedHandler()
  {
    command.execute();
  }
  
  public void keyReleasedHandler()
  {
    command.unExecute();
  }
}
