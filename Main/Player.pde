class Player extends GraphicObject
{
  boolean up, down, left, right;
  float moveX, moveY;
  PVector force;
  
  PImage spriteSheet;
  String spriteSheetImage = "sprite_sheet.png";
  int imgW, imgH, imgX, imgY, imgDir, frameCounter, playerSize, delayAnimation;
  
  Player()
  {
    this.location = new PVector(width/2, height/2);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    this.move = new PVector(0, 0);
    this.drag = new PVector(0, 0);
    this.dragMagnitude = 0;
    this.speed = 2;
    this.topSpeed = 2;
    this.mass = 64;
    this.size = mass * 0.5;
    playerSize = int(size);
    
    spriteSheet = loadImage(spriteSheetImage);
    
    moveX = 0;
    moveY = 0;
    imgW = 400;
    imgH = 600;
    imgX = 0;
    imgY = 0;
    imgDir = 0;
    frameCounter = 0;
    delayAnimation = 0;
    
    up = false;
    down = false;
    left = false;
    right = false;
  }

  void update(float delta)
  {
    velocity.add(acceleration);
    velocity.limit(topSpeed);
    location.add(velocity);
    
    movement(up, down, left, right);
    delayAnimation -= delta;
  }

  void display()
  {
    playerModel();
  }

  void movement(boolean up, boolean down, boolean left, boolean right)
  {
    moveX = (speed * (int(right) - int(left)));
    moveY = (speed * (int(down) - int(up)));
    move = new PVector(moveX, moveY);
    
    if (up || down || left || right)
    {
      if (delayAnimation < 1)
      {
        if (frameCounter < 4)
        {
          frameCounter++;
        }
        else
        {
          frameCounter = 0;
        }
        forward(move, true);
        delayAnimation = 100;
      }
    }
    else
    {
      forward(move, false);
    }

    imgX = (frameCounter % 4) * imgW;
    imgY = imgH * imgDir;
  }
  
  void applyForce(PVector force)
  {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }
  
  void forward(PVector force, boolean state)
  {
    switch (int(state))
    {
      case 0:
      acceleration.set(none);
      velocity.set(none);
      break;
      
      case 1:
      acceleration.add(force);
      break;
    }
  }
  
  void drag()
  {
    dragMagnitude = speed * speed;
    drag = velocity;
    drag.mult(-1);
    drag.mult(dragMagnitude);
    applyForce(drag);
  }

  void playerModel()
  {
    pushMatrix();
    translate(location.x, location.y);
    //collisionZone();
    copy(spriteSheet, imgX, imgY, imgW, imgH, int(location.x - playerSize/2 - Camera.location.x), int(location.y - playerSize/2 - Camera.location.y), playerSize, playerSize);
    popMatrix();
  }
  
  public void moveUp()
  {
    imgDir = 1;
    up = true;
  }
  
  public void moveDown()
  {
    imgDir = 0;
    down = true;
  }
  
  public void moveLeft()
  {
    imgDir = 2;
    left = true;
  }
  
  public void moveRight()
  {
    imgDir = 3;
    right = true;
  }
  
  public void stay()
  {
    up = false;
    down = false;
    left = false;
    right = false;
  }

  //======================
  //  Testing area
  //======================

  void setSpeed(char key, boolean status)
  {
    key = CODED;
    switch (keyCode)
    {
    case SHIFT:
      switch (int(status))
      {
      case 0:
        speed = 2;
        break;

      case 1:
        speed = 4;
        break;
      }
      break;
    }
  }
}
