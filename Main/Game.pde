class Game
{  
  int squareWidth, squareHeight, tileSize;
  ArrayList<PImage> textureList;
  ArrayList<Enemy> enemyList;
  ArrayList<Trap> trapList;
  PVector randomVector;
  
  Tile tile;
  TileMap map;
  TileTexture tileTexture;
  Rectangle tileShape;
  Player player;
  Collider collider;
  RandomGenerator randomGenerator;
  
  EnemyFactory enemyFactory;
  TrapFactory trapFactory;
  
  Invoker invoker;
  ICommand moveUp;
  ICommand moveDown;
  ICommand moveLeft;
  ICommand moveRight;
  HashMap<Character, ICommand> keyInputs;
  
  Game()
  {
    mapSetup();
    player = new Player();
    randomGenerator = new RandomGenerator();
    enemySetup();
    trapSetup();
    collider = new Collider();
    commandsSetup();
  }
  
  void setKeyPressed(char key)
  {
    if (keyInputs.containsKey(key))
    {
      invoker.setCommand(keyInputs.get(key));
      invoker.keyPressedHandler();
    }
  }
  
  void setKeyReleased(char key)
  {
    if (keyInputs.containsKey(key))
    {
      invoker.setCommand(keyInputs.get(key));
      invoker.keyReleasedHandler();
    }
  }
  
  private void mapSetup()
  {
    squareWidth = 24;
    squareHeight = 18;
    tileSize = 32;
    
    map = new TileMap();
    tileShape = new Rectangle();
    tileTexture = new TileTexture();
    textureList = new ArrayList<PImage>();
    
    Tile.setTileTexture(tileTexture.tileSet);
    Tile.setTileShape(tileShape);
    
    for(int i = 0; i < squareHeight; i++)
    {
      for(int j = 0; j < squareWidth; j++)
      {
        Rectangle thisRectangle = Tile.getTileForm(3);
        
        PImage currentTile = Tile.getTileTexture().get(
        (int)thisRectangle.location.x, 
        (int)thisRectangle.location.y,
        (int)thisRectangle.shapeWidth, 
        (int)thisRectangle.shapeHeight);
        
        textureList.add(currentTile);
      }
    }
  }
  
  private void enemySetup()
  {
    enemyFactory = new EnemyFactory();
    enemyList = new ArrayList<Enemy>();
    createEnemies("wizard", 2);
    createEnemies("slime", 3);
  }
  
  private void trapSetup()
  {
    trapFactory = new TrapFactory();
    trapList = new ArrayList<Trap>();
    createTraps("altar", 3);
  }
  
  private void commandsSetup()
  {
    invoker = new Invoker();
    moveUp = new MoveUp(this.player);
    moveDown = new MoveDown(this.player);
    moveLeft = new MoveLeft(this.player);
    moveRight = new MoveRight(this.player);
    
    keyInputs = new HashMap();
    keyInputs.put('w', moveUp);
    keyInputs.put('s', moveDown);
    keyInputs.put('a', moveLeft);
    keyInputs.put('d', moveRight);
  }
  
  private void createEnemies(String type, int quantity)
  {
    for (int i = 0; i < quantity; i++)
    {
      randomGenerator.generate(player.location);
      randomVector = randomGenerator.getRandomVector();
      switch (type)
      {
        case "wizard":
        enemyList.add(enemyFactory.create("wizard", randomVector));
        break;
        
        case "slime":
        enemyList.add(enemyFactory.create("slime", randomVector));
        break;
      }
    }
  }
  
  private void createTraps(String type, int quantity)
  {
    for (int i = 0; i < quantity; i++)
    {
      randomGenerator.generate(player.location);
      randomVector = randomGenerator.getRandomVector();
      switch (type)
      {
        case "altar":
        trapList.add(trapFactory.create("altar", randomVector));
        break;
      }
    }
  }
  
  void update(int delta)
  {
    player.update(delta);
    enemyUpdate(delta);
    tileDetector();
    for (int i = 0; i < enemyList.size(); i++)
    {
      collider.circleToCirle(player.location, enemyList.get(i).location, enemyList.get(i), player.size / 2, enemyList.get(i).size / 2);
    }
  }
  
  void enemyUpdate(int delta)
  {
    for (int i = 0; i < enemyList.size(); i++)
    {
      enemyList.get(i).update(delta);
    }
  }
  
  void display()
  {
    cameraDisplay();
    mapDisplay();
    player.display();
    enemyDisplay();
    trapDisplay();
    
    //println(Math.floor(player.location.x / 32));
    //println(Math.floor(player.location.y / 32));
  }
  
  void cameraDisplay()
  {
    PVector cameraLocation = player.location.get();
    cameraLocation.x -= width/2;
    cameraLocation.y -= height/2;
    Camera.setLocation(cameraLocation);
    
    PVector minLimit = new PVector(0, 0);
    PVector maxLimit = new PVector(width + 200, height + 200);
    Camera.location.x = Math.max(minLimit.x - 200, Camera.location.x);
    Camera.location.x = Math.min(maxLimit.x - width, Camera.location.x);
    Camera.location.y = Math.max(minLimit.y - 200, Camera.location.y);
    Camera.location.y = Math.min(maxLimit.y - height, Camera.location.y);
  }
  
  void mapDisplay()
  {
    for (int i = 0; i < squareHeight; i++)
    {
      int positionY = (i * tileSize);
      for (int j = 0; j < squareWidth; j++)
      {
        image(textureList.get(i + j), (j * tileSize) - Camera.location.x, positionY - Camera.location.y);
      }
    }
  }
  
  void enemyDisplay()
  {
    for (int i = 0; i < enemyList.size(); i++)
    {
      enemyList.get(i).display(); 
    }
  }
  
  void trapDisplay()
  {
    for (int i = 0; i < trapList.size(); i++)
    {
      trapList.get(i).display();
    }
  }
  
  //======================
  //  Testing
  //======================
  
  void tileDetector()
  {
    /*
    if ((player.location.x > thisRect.location.x) 
    && (player.location.x < thisRect.location.x + thisRect.getShapeWidth()) 
    && (player.location.y > thisRect.location.y) 
    && (player.location.y < thisRect.location.y + thisRect.getShapeHeight()))
    {
      println("INSIDE");
    }
    else
    {
      println("-------");
    }
    */
  }
}
