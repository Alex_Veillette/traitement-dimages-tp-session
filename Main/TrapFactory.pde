public class TrapFactory extends Factory {
 
  public Trap create(String name, PVector location) {
    Trap trap = null;
    
    switch (name) {
      case "altar":
        trap = new Altar(location, color(20, 20, 20));
        break;
    }
    return trap; 
  }
}
