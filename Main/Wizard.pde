class Wizard extends Enemy
{
  Wizard(PVector location, PVector velocity, color fillColor)
  {
    this.location = location;  
    this.velocity = velocity;
    this.acceleration = new PVector(0, 0);

    this.mass = 64;
    this.size = mass * 0.5;

    this.baseFillColor = fillColor;
    this.fillColor = fillColor;
    createProjectiles(10);
    println("wizard created");
  }
  
  void update(float delta)
  {
    projectileUpdate(delta);
  }

  void display()
  {
    enemyModel();
    projectileDisplay();
  }
  
  void createProjectiles(int quantity)
  {
    for (int i = 0; i < quantity; i++)
    {
      Projectile projectile = new Fireball(location, color(250, 20, 20));
      projectileList.add(projectile);
    }
  }
  
  void setColor(color fillColor) {
    
    this.fillColor = fillColor;
  }
  
  void enemyModel()
  {
    pushMatrix();    
    translate(location.x  - Camera.location.x, location.y  - Camera.location.y);
    fill(fillColor);
    ellipse (0, 0, size, size);
    popMatrix();
  }
}
