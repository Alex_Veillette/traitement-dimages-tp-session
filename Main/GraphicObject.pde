abstract class GraphicObject
{
  PVector location;
  PVector velocity;
  PVector acceleration;
  PVector move;
  float size;
  float speed, topSpeed;
  float mass;
  float G; // Gravitational Constant
  
  PVector none = new PVector(0, 0);
  PVector drag;
  float dragMagnitude;
  
  color fillColor = color(255);
  color baseFillColor = color(255);
  color strokeColor = color(0);
  float strokeWeight = 1;
  
  abstract void update(float deltaTime);
  
  abstract void display();
}
