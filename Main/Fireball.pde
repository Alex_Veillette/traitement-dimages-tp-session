public class Fireball extends Projectile
{
  float projectileDist;
  float angle, rAngle;
  
  float topSteer = 0.03;
  
  float theta = 0;
  
  float radiusSeparation;
  float radiusAlignment;
  float radiusCohesion;

  float weightSeparation = 1.5;
  float weightAlignment = 1;
  float weightCohesion = 1;
  
  PVector steer;
  PVector sum;
  PVector sumAlignment;
  PVector sumCohesion;
  
  PVector zeroVector = new PVector(0, 0);
  public Fireball(PVector location, color fillColor)
  {
    projectileDist = 40;
    this.location.x = random(location.x - projectileDist, location.x + projectileDist);
    this.location.y = random(location.y - projectileDist, location.y + projectileDist);
    
    this.mass = 16;
    this.size = mass * 0.5;
    this.G = 0.5;    
    
    radiusSeparation = 1 * mass;
    radiusAlignment = 20 * mass;
    radiusCohesion = 30 * mass;
    
    this.fillColor = fillColor;
    
    println("fireball created");
  }
  
  void update(float delta)
  {
    velocity.add (acceleration);
    velocity.limit(topSpeed);
    location.add (velocity);

    acceleration.mult (0); 
  }
  
  void display()
  {
    projectileModel();
  }
  
  void projectileModel()
  {
    theta = velocity.heading() + radians(90);
    
    pushMatrix();
    translate(location.x  - Camera.location.x, location.y  - Camera.location.y);
    rotate (theta);
    fill(fillColor);
    ellipse (projectileDist, projectileDist, size, size);
    popMatrix();
  }
  
  void applyForce(PVector force) 
  {
    PVector f = PVector.div(force,mass);
    acceleration.add(f);
  }
  
  PVector attract(Projectile projectile) {
   
    PVector force = PVector.sub(location, projectile.location);
    float distance = force.mag();
    distance = constrain(distance, 5.0, 25.0);
    force.normalize();
    
    float strength = (G * mass * projectile.mass) / (distance * distance);
    force.mult(strength);
    
    return force;
  }
  
  PVector repel(Projectile projectile) {
    PVector force = PVector.sub(location ,projectile.location);
    float distance = force.mag();
    distance = constrain(distance, 100.0, 1000.0);
    force.normalize();

    float strength = (G * mass * projectile.mass) / (distance * distance);
    force.mult(-1 * strength);
    return force;
  }
  
  void flock(ArrayList<Projectile> projectiles) 
  {
    PVector separation = separate(projectiles);
    PVector alignment = align(projectiles);
    PVector cohesion = cohesion(projectiles);
    
    separation.mult(weightSeparation);
    alignment.mult(weightSeparation);
    cohesion.mult(weightCohesion);

    applyForce(separation);
    applyForce(alignment);
    applyForce(cohesion);
  }
  
  PVector separate (ArrayList<Projectile> projectiles) 
  {
    PVector steer = new PVector(0, 0, 0);
    
    int count = 0;
    
    for (GraphicObject other : projectiles) 
    {
      float d = PVector.dist(location, other.location);
      
      if (d > 0 && d < radiusSeparation) 
      {
        PVector diff = PVector.sub(location, other.location);
        
        diff.normalize();
        diff.div(d);
        
        steer.add(diff);
        
        count++;
      }
    }
    
    if (count > 0) 
    {
      steer.div(count);
    }
    
    if (steer.mag() > 0) 
    {
      steer.setMag(topSpeed);
      steer.sub(velocity);
      steer.limit(topSteer);
    }
    
    return steer;
  }
  
  PVector align (ArrayList<Projectile> projectiles) 
  {

    if (sum == null) 
    {
      sum = new PVector();      
    } 
    else 
    {
      sum.mult(0);
    }

    int count = 0;

    for (Projectile other : projectiles) 
    {
      float d = PVector.dist(this.location, other.location);

      if (d > 0 && d < radiusAlignment) 
      {
        sum.add(other.velocity);
        count++;
      }
    }

    if (count > 0) 
    {
      sum.div((float)count);
      sum.setMag(topSpeed);

      PVector steer = PVector.sub(sum, this.velocity);
      steer.limit(topSteer);

      return steer;
    } 
    else 
    {
      return new PVector();
    }
  }
  
  PVector seek (PVector target) 
  {
    // Vecteur différentiel vers la cible
    PVector desired = PVector.sub (target, this.location);
    
    // VITESSE MAXIMALE VERS LA CIBLE
    desired.setMag(topSpeed);
    
    // Braquage
    PVector steer = PVector.sub (desired, velocity);
    steer.limit(topSteer);
    
    return steer;    
  }
  
  PVector cohesion (ArrayList<Projectile> projectiles) 
  {
    if (sumCohesion == null) 
    {
      sumCohesion = new PVector();      
    } 
    else 
    {
      sumCohesion.mult(0);
    }

    int count = 0;

    for (Projectile other : projectiles) 
    {
      float d = PVector.dist(location, other.location);

      if (d > 0 && d < radiusCohesion) 
      {
        sumCohesion.add(other.location);
        count++;
      }
    }

    if (count > 0) 
    {
      sumCohesion.div(count);

      return seek(sumCohesion);
    } 
    else 
    {
      return zeroVector;
    }
    
  }
}
