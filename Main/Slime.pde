class Slime extends Enemy
{
  Slime(PVector location, PVector velocity, color fillColor)
  {
    this.location = location;  
    this.velocity = velocity;
    this.acceleration = new PVector(0, 0);

    this.mass = 48;
    this.size = mass * 0.5;

    this.baseFillColor = fillColor;
    this.fillColor = fillColor;
    createProjectiles(4);
    println("slime created");
  }
  
  void update(float delta)
  {
    projectileUpdate(delta);
  }

  void display()
  {
    enemyModel();
    projectileDisplay();
  }
  
  void createProjectiles(int quantity)
  {
    for (int i = 0; i < quantity; i++)
    {
      Projectile projectile = new Fireball(location, color(100, 250, 100));
      projectileList.add(projectile);
    }
  }
  
  void setColor(color fillColor) {
    
    this.fillColor = fillColor;
  }
  
  void enemyModel()
  {
    pushMatrix();    
    translate(location.x  - Camera.location.x, location.y  - Camera.location.y);
    fill(fillColor);
    ellipse (0, 0, size, size);
    popMatrix();
  }
}
