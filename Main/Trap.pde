abstract class Trap extends GraphicObject
{
  
  Trap()
  {
    this.location = new PVector(0, 0);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    
    this.mass = 32;
    this.size = mass * 0.5;
  }
  
  void update(float delta)
  {
    
  }
  
  void display()
  {
    
  }
  
  abstract void createProjectiles(int quantity);
}
