public static class Tile
{
  static PImage tileSet;
  static int tileWidth = 32, tileHeight = 32, tileX = 0, tileY = 0;
  static Rectangle tileShape;
  
  public static Rectangle getTileForm(int index)
  {
    tileX = index % (tileSet.width / tileWidth);
    tileY = index / (tileSet.width / tileWidth);
    
    tileShape.setShapeWH(tileWidth, tileHeight);
    tileShape.setShapeXY(tileX * tileWidth, tileY * tileHeight);
    
    return tileShape;
  }
  
  public static PImage getTileTexture()
  {
    return tileSet;
  }
  
  public static int getTileWidth()
  {
    return tileWidth;
  }
  
  public static int getTileHeight()
  {
    return tileHeight;
  }
  
  public static int getTileX()
  {
    return tileX;
  }
  
  public static int getTileY()
  {
    return tileY;
  }
  
  public static void setTileShape(Rectangle _tileShape)
  {
    tileShape = _tileShape;
  }
  
  
  public static void setTileTexture(PImage _tileSet)
  {
    tileSet = _tileSet;
  }
  
  public static void setTileWidth(int _tileWidth)
  {
    tileWidth = _tileWidth;
  }
  
  public static void setTileHeight(int _tileHeight)
  {
    tileHeight = _tileHeight;
  }
}
