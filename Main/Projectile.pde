abstract class Projectile extends GraphicObject
{
  
  Projectile()
  {
    this.location = new PVector(0, 0);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    this.topSpeed = 2;
    
    this.mass = 8;
    this.size = mass * 0.5;
  }
  
  void update(float delta)
  {
    velocity.add (acceleration);
    velocity.limit(topSpeed);
    location.add (velocity);

    acceleration.mult (0); 
  }
  
  void display()
  {
    
  }
  
  abstract void applyForce(PVector force);
  
  abstract PVector attract(Projectile projectile);
  
  abstract PVector repel(Projectile projectile);
  
  abstract void flock(ArrayList<Projectile> projectiles);
  
  abstract PVector separate (ArrayList<Projectile> projectiles);
  
  abstract PVector align (ArrayList<Projectile> projectiles);
  
  abstract PVector seek (PVector target) ;
  
}
