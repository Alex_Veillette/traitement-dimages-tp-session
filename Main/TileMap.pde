public class TileMap
{
  private ArrayList<TileMapRow> rows;
  private TileMapRow row;
  private int mapWidth, mapHeight;
  
  public TileMap()
  {
    rows = new ArrayList<TileMapRow>();
    mapWidth = 100;
    mapHeight = 100;
    
    mapInit();
  }
  
  public TileMapRow getMapRow(int index)
  {
    return rows.get(index);
  }
  
  public int getMapWidth()
  {
    return mapWidth;
  }
  
  public int getMapHeight()
  {
    return mapHeight;
  }
  
  public void setMapWidth(int mapWidth)
  {
    this.mapWidth = mapWidth;
  }
  
  public void setMapHeight(int mapHeight)
  {
    this.mapHeight = mapHeight;
  }
  
  public void mapInit()
  {
    for (int i = 0; i < mapHeight; i++)
    {
      row = new TileMapRow();
      for (int j = 0; j < mapWidth; j++)
      {
        row.columns.add(new TileMapCell(0));
      }
      rows.add(row);
    }
  }
}
