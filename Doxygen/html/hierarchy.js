var hierarchy =
[
    [ "Main.Camera", "class_main_1_1_camera.html", null ],
    [ "Main.Collider", "class_main_1_1_collider.html", null ],
    [ "Main.Factory", "class_main_1_1_factory.html", [
      [ "Main.EnemyFactory", "class_main_1_1_enemy_factory.html", null ],
      [ "Main.TrapFactory", "class_main_1_1_trap_factory.html", null ]
    ] ],
    [ "Main.Game", "class_main_1_1_game.html", null ],
    [ "Main.GraphicObject", "class_main_1_1_graphic_object.html", [
      [ "Main.Enemy", "class_main_1_1_enemy.html", [
        [ "Main.Slime", "class_main_1_1_slime.html", null ],
        [ "Main.Wizard", "class_main_1_1_wizard.html", null ]
      ] ],
      [ "Main.Player", "class_main_1_1_player.html", null ],
      [ "Main.Projectile", "class_main_1_1_projectile.html", [
        [ "Main.Fireball", "class_main_1_1_fireball.html", null ]
      ] ],
      [ "Main.Rectangle", "class_main_1_1_rectangle.html", null ],
      [ "Main.Trap", "class_main_1_1_trap.html", [
        [ "Main.Altar", "class_main_1_1_altar.html", null ]
      ] ]
    ] ],
    [ "Main.ICommand", "interface_main_1_1_i_command.html", [
      [ "Main.MoveDown", "class_main_1_1_move_down.html", null ],
      [ "Main.MoveLeft", "class_main_1_1_move_left.html", null ],
      [ "Main.MoveRight", "class_main_1_1_move_right.html", null ],
      [ "Main.MoveUp", "class_main_1_1_move_up.html", null ]
    ] ],
    [ "Main.Invoker", "class_main_1_1_invoker.html", null ],
    [ "Main.MoveCommand", "class_main_1_1_move_command.html", null ],
    [ "PApplet", null, [
      [ "Main", "class_main.html", null ]
    ] ],
    [ "Main.RandomGenerator", "class_main_1_1_random_generator.html", null ],
    [ "Main.Tile", "class_main_1_1_tile.html", null ],
    [ "Main.TileMap", "class_main_1_1_tile_map.html", null ],
    [ "Main.TileMapCell", "class_main_1_1_tile_map_cell.html", null ],
    [ "Main.TileMapRow", "class_main_1_1_tile_map_row.html", null ],
    [ "Main.TileTexture", "class_main_1_1_tile_texture.html", null ]
];